#include <stdio.h>
#include <stdlib.h>
#include "estudiantes.h"
#include <math.h>
#include <string.h>

float desvStd(estudiante curso[]){
	float pm = 0, base, dif, dif2, desv = 0;
	int i=0, cont=0, sum;
	while(curso[i].prom != 0.0){
		pm += curso[i].prom;
		i++;
		cont++;
	}
	if (cont == 0){
		printf("No se han ingresado notas al sistema\n");
		return 0;
	}
	pm=pm/cont;
	i=0;
	while(i<cont){
		dif=curso[i].prom-pm;
		dif2=dif*dif;
		sum+=dif2;
		i++;
	}
	base=sum/cont;
	desv=sqrt(base);
	return desv;
}

float menor(estudiante curso[]){
	float min = 7.0;
	int i = 0, cont=0;
	while(strcmp(curso[i].nombre, "\0")!=0){
		i++;
		cont++;
	i=0;
	int alm=cont;
	for(i=0, i<alm, i++){
		if(min>curso[i].prom && curso[i].prom != 0){
			menor=curso[i].prom;
		}
	}
	return menor;
	}

}

float mayor(estudiante curso[]){
	float maj=1.0;
	int i=0;
	while(curso[i].prom!=0){
		if(maj<curso[i].prom){
			maj = curso[i].prom;
		}
		i++;
	}
	return maj;
}

void registroCurso(estudiante curso[]){
	int i, prom[24], pm_parcial;
	for (i=0, i<24, i++);{
		curso[i].prom=0;
		printf("-------------------");
		printf("El estudiante para ingresar notas es el siguiente: %s %s %s \n", curso[i].nombre,curso[i].apellidoP,curso[i].apellidoM);
		printf("Ingrese la nota del Proyecto N1:\n");
		scanf("%f", &curso[i].asig_1.proy1);
		pm_parcial=pm_parcial+(curso[i].asig_1.proy1 * 0.20);
		printf("Ingrese la nota del Proyecto N2:\n");
		scanf("%f", &curso[i].asig_1.proy2);
		pm_parcial=pm_parcial+(curso[i].asig_1.proy2 * 0.20);
		printf("Ingrese la nota del Proyecto N3:\n");
		scanf("%f", &curso[i].asig_1.proy3);
		pm_parcial=pm_parcial+(curso[i].asig_1.proy3 * 0.30);
	}
	printf("TEST"); 

}

void clasificarEstudiantes(char path[], estudiante curso[]){
	printf("TEST"); 
}


void metricasEstudiantes(estudiante curso[]){
	printf("TEST"); 
}





void menu(estudiante curso[]){
	int opcion;
    do{
		printf( "\n   1. Cargar Estudiantes" );
		printf( "\n   2. Ingresar notas" );
		printf( "\n   3. Mostrar Promedios" );
		printf( "\n   4. Almacenar en archivo" );
		printf( "\n   5. Clasificar Estudiantes " );
		printf( "\n   6. Salir." );
		printf( "\n\n   Introduzca opción (1-6): " );
		scanf( "%d", &opcion );
        switch ( opcion ){
			case 1: cargarEstudiantes("estudiantes.txt", curso); //carga en lote una lista en un arreglo de registros
					break;

			case 2:  registroCurso(curso);// Realiza ingreso de notas en el registro curso 
					break;

			case 3: metricasEstudiantes(curso); //presenta métricas de disperción, tales como mejor promedio; más bajo; desviación estándard.
					break;

			case 4: grabarEstudiantes("test.txt",curso);
					break;
            case 5: clasificarEstudiantes("destino", curso); // clasi
            		break;
         }

    } while ( opcion != 6 );
}

int main(){
	estudiante curso[30]; 
	menu(curso); 
	return EXIT_SUCCESS; 
}